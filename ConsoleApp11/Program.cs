﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HtmlAgilityPack;
using JetBrains.Annotations;

namespace ConsoleApp11
{
	class Program
	{
		static void Main()
		{
			var x = new EmailBuilder();
			x.AddStuff();
			x.Send();
		}
	}

	/// <summary>
	/// Email builder class.
	/// </summary>
	public class EmailBuilder
	{
		private const string BaseEmail = "<!DOCTYPE html><html><body><div style=\"margin: 4px 4px 26px 4px\"><h3>SAM Update Report: syncedDateTime</h3></div></body></html>";
		private const string BaseTitle = "<div style=\"margin: 0 4px  4px 4px\"><strong></strong></div>";
		private const string BaseTable =
			"<div style=\"margin: 0 4px 36px 4px\">" +
			"<table border=\"1\" cellpadding=\"5\" cellspacing=\"0\" style=\"bordercollapse:collapse;color:black;font-size:12px;font-family:arial,helvetica,sans-serif;\">" +
			"<tr>" +
			"<th style=\"text-align:center;background-color:#B5121B;color:White;\">Emp No.</th>" +
			"<th style=\"text-align:center;background-color:#B5121B;color:White;\">First Name</th>" +
			"<th style=\"text-align:center;background-color:#B5121B;color:White;\">Last Name</th>" +
			"<th style=\"text-align:center;background-color:#B5121B;color:White;\">Job Title</th>" +
			"<th style=\"text-align:center;background-color:#B5121B;color:White;\">Status</th>" +
			"<th style=\"text-align:center;background-color:#B5121B;color:White;\">Start Date</th>" +
			"<th style=\"text-align:center;background-color:#B5121B;color:White;\">End Date</th>" +
			"</tr>" +
			"</table>" + 
			"</div>";
		private const string BaseTableRow = "<tr><td>EmpNo</td><td>FirstName</td><td>LastName</td><td>JobTitle</td><td>Status</td><td>StartDAte</td><td>EndDate</td></tr>";

		private DateTime _syncedDateTime = DateTime.Now;
		[NotNull] private readonly HtmlDocument _email = new HtmlDocument();
		[NotNull] private readonly List<EmailTable> _samNewHire = new List<EmailTable>();
		[NotNull] private readonly List<EmailTable> _updateStatus = new List<EmailTable>();

		/// <summary>
		/// Send email.
		/// </summary>
		public void Send()
		{
			var t = BaseEmail.Replace("syncedDateTime", _syncedDateTime.ToLongDateString());
			_email.LoadHtml(t);
			AppendTable(_samNewHire, "New Hire");
			AppendTable(_updateStatus, "Status Update");
		}

		private void AppendTable([NotNull] List<EmailTable> emailTable, [CanBeNull] string title = null)
		{
			if (!emailTable.Any())
			{
				return;
			}

			var reportNode = _email.DocumentNode.SelectSingleNode("//body");
			if (!string.IsNullOrEmpty(title))
			{
				var tableTitleNode = HtmlNode.CreateNode(BaseTitle);
				tableTitleNode.InnerHtml = title;
				reportNode.AppendChild(tableTitleNode);
			}

			var tableHtmlNode = HtmlNode.CreateNode(BaseTable);
			var tableDataNode = tableHtmlNode.SelectSingleNode("//table");
			foreach (var row in emailTable)
				{
				var s = new StringBuilder(BaseTableRow);
				s.Replace("EmpNo", row.EmployeeNumber);
				s.Replace("FirstName", row.FirstName);
				s.Replace("LastName", row.LastName);
				s.Replace("JobTitle", row.JobTitle);
				s.Replace("Status", row.Status);
				s.Replace("StartDAte", row.StartDateTimeString);
				s.Replace("EndDate", row.EndDateTimeString);
				var newRow = HtmlNode.CreateNode(s.ToString());
				tableDataNode.AppendChild(newRow);
			}

			reportNode.AppendChild(tableHtmlNode);
		}

		/// <summary>
		/// Add pretend data.
		/// </summary>
		public void AddStuff()
		{
			var p1 = new EmailTable { EmployeeNumber = "123454", FirstName = "Sam", JobTitle = "Gaurd", LastName = "Loo", StartDateTimeString = DateTime.Today.ToShortDateString(), Status = "Active" };
			var p2 = new EmailTable { EmployeeNumber = "224947", FirstName = "Don", JobTitle = "Teller", LastName = "Jones", StartDateTimeString = "5/12/2013", Status = "Terminated", EndDateTimeString = DateTime.Today.ToShortDateString() };
			var p3 = new EmailTable { EmployeeNumber = "975107", FirstName = "Susand", JobTitle = "Teller Manager", LastName = "Slaughter", StartDateTimeString = DateTime.Today.ToShortDateString(), Status = "Active" };
			var p4 = new EmailTable { EmployeeNumber = "924524", FirstName = "James", JobTitle = "Loan Officer", LastName = "Donaway", StartDateTimeString = "6/8/2003", Status = "Terminated", EndDateTimeString = "5/5/2017" };
			var p5 = new EmailTable { EmployeeNumber = "954247", FirstName = "Russell", JobTitle = "Software Developer", LastName = "Ebbing", StartDateTimeString = "1/30/2017", Status = "Active" };
			var p6 = new EmailTable { EmployeeNumber = "01678d", FirstName = "Terry", JobTitle = "Call Center", LastName = "Kozak", StartDateTimeString = "3/5/2012", Status = "Active" };

			_samNewHire.Add(p1);
			_samNewHire.Add(p3);
			_samNewHire.Add(p5);
			_samNewHire.Add(p6);
			_updateStatus.Add(p2);
			_updateStatus.Add(p4);
		}

	}

	internal class EmailTable
	{
		private const string NoDateTimeText = "No Date";
		private const string NoValueText = "No Value";
		[NotNull]
		public string EmployeeNumber { get; set; } = NoValueText;
		[NotNull]
		public string EndDateTimeString { get; set; } = NoDateTimeText;
		[NotNull]
		public string FirstName { get; set; } = NoValueText;
		[NotNull]
		public string JobTitle { get; set; } = NoValueText;
		[NotNull]
		public string LastName { get; set; } = NoValueText;
		[NotNull]
		public string StartDateTimeString { get; set; } = NoDateTimeText;
		[NotNull]
		public string Status { get; set; } = NoValueText;
	}
}
